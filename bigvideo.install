<?php
/**
 * @file
 * Module install hooks.
 */

/**
 * Implements hook_requirements().
 */
function bigvideo_requirements($phase) {
  $requirements = array();

  if ($phase === 'runtime') {
    $t = get_t();

    $requirements['bigvideo'] = array(
      'title' => $t('BigVideo'),
      'weight' => 100,
      'severity' => REQUIREMENT_OK,
      'value' => $t('BigVideo.js and Imagesloaded are installed'),
    );

    $bigvideo = libraries_detect('bigvideojs');
    $imagesloaded = libraries_detect('imagesloaded');

    if (!$bigvideo['installed'] && !$imagesloaded['installed']) {
      $requirements['bigvideo']['severity'] = REQUIREMENT_ERROR;
      $requirements['bigvideo']['value'] = $t('BigVideo.js and imagesLoaded are not installed');
    }
    elseif (!$bigvideo['installed']) {
      $requirements['bigvideo']['severity'] = REQUIREMENT_ERROR;
      $requirements['bigvideo']['value'] = $t('BigVideo.js is not installed');
    }
    elseif (!$imagesloaded['installed']) {
      $requirements['bigvideo']['severity'] = REQUIREMENT_ERROR;
      $requirements['bigvideo']['value'] = $t('imagesLoaded is not installed');
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function bigvideo_schema() {
  return array(
    'bigvideo_source' => array(
      'description' => 'Bigvideo sources',
      'fields' => array(
        'bsid' => array(
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not_null' => TRUE,
        ),
        'type' => array(
          'type' => 'int',
          'length' => 1,
          'not_null' => TRUE,
        ),
        'mp4' => array(
          'type' => 'text',
        ),
        'webm' => array(
          'type' => 'text',
        ),
      ),
      'primary key' => array('bsid'),
    ),
    'bigvideo_page' => array(
      'description' => 'Bigvideo pages',
      'fields' => array(
        'bpid' => array(
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not_null' => TRUE,
        ),
        'source' => array(
          'type' => 'int',
        ),
        'path' => array(
          'type' => 'text',
        ),
        'selector' => array(
          'type' => 'text',
        ),
        'status' => array(
          'type' => 'int',
          'length' => 1,
        ),
      ),
      'primary key' => array('bpid'),
    ),
  );
}

/**
 * Add "selector" field to bigvideo_page table.
 */
function bigvideo_update_7101() {
  db_add_field('bigvideo_page', 'selector', array(
    'type' => 'text',
  ));
}
